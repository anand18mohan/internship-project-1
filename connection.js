require("dotenv").config();
const { createConnection } = require("mysql");

const sqlConn = createConnection({
    host: process.env.SQLHOST,
    database: process.env.SQLDB,
    user: process.env.SQLUSER,
    password: process.env.SQLPASS
});

sqlConn.connect((e) => {
    if(e) {
        console.log(`Connection failed: ${e}`);
        return;
    }
    console.log("Connected to database");
})

module.exports = sqlConn;