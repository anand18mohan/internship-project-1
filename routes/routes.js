const express = require('express');
const router = express.Router();
require("dotenv").config();
const {
    queryRunner
} = require("../functions/queryRunner");
const {
    isAuth
} = require("../functions/authentication");

module.exports = router;

router.get('/checkstatus', (req, res) => {
    res.status(200).send({
        message: "SERVER ONLINE"
    });
});

router.get('/tasks', isAuth, async (req, res) => {

    let result = await queryRunner(`SELECT * FROM tasks`);
    console.log(result);
    if (result.success)
        res.status(200).send(
            result.result
        );
});

router.post('/tasks', isAuth, async (req, res) => {
    console.log(req.body)
    let task_name = req.body.task_name;
    if (!task_name) {
        res.status(400).send({
            message: "Invalid Request Body"
        });
        return;
    }
    let result = await queryRunner(`INSERT INTO tasks(task_name) VALUES ('${task_name}')`);
    if (result.success) {
        let query = await queryRunner(`SELECT * FROM tasks WHERE id=${result.result.insertId}`);
        res.status(200).send(
            query.result
        );
    } else {
        res.status(400).send({
            message: "Error while running query",
            result: result.result
        });
    }
});

router.patch('/tasks/:id', isAuth, async (req, res) => {
    const taskId = req.params.id;
    let completed = req.body.completed;
    if (!completed && !(completed === false || completed === true)) {
        res.status(400).send({
            message: "Invalid Request Body"
        });
        return;
    }
    let result = await queryRunner(`UPDATE tasks SET completed=${completed} WHERE id=${taskId}`);
    if (result.success && result.result.affectedRows > 0) {
        let query = await queryRunner(`SELECT * FROM tasks WHERE id=${taskId}`);
        res.status(200).send(
            query.result
        );
    } else if (result.result.affectedRows === 0) {
        res.status(400).send({
            message: "Task does not exist",
        });
    } else {
        res.status(400).send({
            message: "Error while running query",
            result: result.result
        });
    }
});

router.delete('/tasks/:id', isAuth, async (req, res) => {
    const taskId = req.params.id;

    let result = await queryRunner(`DELETE FROM tasks WHERE id=${taskId}`);
    if (result.success && result.result.affectedRows > 0) {
        res.status(200).send({
            message: `task ${taskId} deleted successfully`
        });
    } else if (result.result.affectedRows === 0) {
        res.status(400).send({
            message: "Task does not exist",
        });
    } else {
        res.status(400).send({
            message: "Error while running query",
            result: result.result
        });
    }
});
