## Database Create Command
```
CREATE TABLE tasks (
  id INT AUTO_INCREMENT PRIMARY KEY,
  task_name VARCHAR(255),
  completed BOOLEAN DEFAULT FALSE,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
```

## Set .env 
PORT=
API_KEY= 
CORS=
SQLHOST=
SQLDB=
SQLUSER=
SQLPASS=

## Install Packages
```npm install```

## Run Server
```npm start```
npm start runs `nodemon index.js` 

## ENDPOINTS
GET /tasks
POST /tasks
PATCH /tasks/:id
DELETE /tasks/:id