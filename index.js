const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const winston = require('winston');
const expressWinston = require('express-winston');
require('dotenv').config();
require("./connection");

const corsOptions = {
    origin: process.env.CORS,
    credentials: true,
    optionSuccessStatus: 200,
}

const app = express();
const PORT = process.env.PORT || 3000;
app.use(express.json());
app.use(helmet());
app.use(cors(corsOptions));

app.use(expressWinston.logger({
    transports: [
        new winston.transports.File({
            filename: "./access.log"
        })
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    ),
    meta: true,
    msg: "HTTP {{req.method}} {{req.url}}",
    expressFormat: true,
    colorize: false,
    ignoreRoute: function(req, res) {
        return false;
    }
}));


app.listen(PORT, () => {
    console.log(`Server started at ${PORT}`)
})

const routes = require('./routes/routes');

app.use(routes)