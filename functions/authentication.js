require("dotenv").config();

async function isAuth(req, res, next) {
    const auth = req.headers.apikey;
    if (auth !== process.env.API_KEY || !auth) {
        res.status(401);
        res.send("Invalid Api Key");
    } else {
        next();
    }
}

module.exports = { isAuth };