const sqlConn = require("../connection");

async function queryRunner(query) {
    try {
      const result = await new Promise((resolve, reject) => {
        sqlConn.query(query, (err, result) => {
          if (err) {
            console.log(err);
            return reject(err);
          }
          resolve(result);
        });
      });
  
      return { result, success: true };
    } catch (error) {
      return { result: error, success: false };
    }
  }
  

module.exports = {
    queryRunner
}